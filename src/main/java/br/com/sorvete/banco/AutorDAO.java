package br.com.sorvete.banco;

import br.com.sorvete.entity.Autor;

public class AutorDAO extends DAO<Autor> {

    public AutorDAO() {
        super(Autor.class);
    }

}
